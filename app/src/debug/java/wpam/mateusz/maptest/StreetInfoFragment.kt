package wpam.mateusz.maptest


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.provider.AlarmClock
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import wpam.mateusz.maptest.activities.AddOpinionActivity
import wpam.mateusz.maptest.activities.ListOpinionsActivity
import wpam.mateusz.maptest.model.Street

/**
 * A simple [Fragment] subclass.
 */
class StreetInfoFragment : Fragment() {

    var selectedStreet: Int? = null
    private val database = FirebaseDatabase.getInstance().reference

    private val ADD_RESULT_CODE = 1

    lateinit var addButton: FloatingActionButton
    lateinit var streetTextView: TextView
    lateinit var ratingBar: RatingBar
    lateinit var opinionsButton: Button
    lateinit var commentsCountText: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_street_info, container, false)

        addButton = rootView.findViewById<FloatingActionButton>(R.id.addActionButton)
        streetTextView = rootView.findViewById<TextView>(R.id.streetNameTextView)
        ratingBar = rootView.findViewById<RatingBar>(R.id.streetRatingBar)
        opinionsButton = rootView.findViewById<Button>(R.id.opinionsButton)
        commentsCountText = rootView.findViewById<TextView>(R.id.commentsCountTextView)


        addButton.setOnClickListener {
            val id = selectedStreet
            if (id != null) {
                val intent = Intent(
                    rootView.context,
                    AddOpinionActivity::class.java
                ).apply {
                    putExtra(AlarmClock.EXTRA_MESSAGE, id)
                }
                startActivityForResult(intent, ADD_RESULT_CODE)
            }
        }

        opinionsButton.setOnClickListener {
            val id = selectedStreet
            if (id != null) {
                val intent = Intent(
                    rootView.context,
                    ListOpinionsActivity::class.java
                ).apply {
                    putExtra(AlarmClock.EXTRA_MESSAGE, id)
                }
                startActivity(intent)
            }
        }

        database.child("streets").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val id = selectedStreet
                id ?: return

                val streetData = dataSnapshot.child(id.toString())
                val street = streetData.getValue(Street::class.java)
                if (street != null) {
                    Log.w("TAG", "FragmentStreetInfo: " + street.toString())
                    updateStreetInfo(street)
                    return
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                Log.w("TAG", "streetLoad: onCancelled", databaseError.toException())
            }
        })

        database.child("streets").child(selectedStreet.toString()).child("opinions")
            .addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val opinionsCount = dataSnapshot.children.count()
                    updateOpinionCount(opinionsCount)
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    Log.w("TAG", "opinionsCount: onCancelled", databaseError.toException())
                }
            })

        return rootView
    }

    private fun updateStreetInfo(street: Street) {
        streetTextView.text = street.address.name
        ratingBar.rating = street.rating
    }

    private fun updateOpinionCount(opinionsCount: Int) {
        commentsCountText.text = opinionsCount.toString()
        if (opinionsCount > 0) {
            opinionsButton.isEnabled = true
        }
    }

}
