package wpam.mateusz.maptest.adapters

import android.content.Context
import android.media.Rating
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import org.w3c.dom.Text
import wpam.mateusz.maptest.R
import wpam.mateusz.maptest.model.Opinion

class OpinionAdapter(private val context: Context, private val dataSource: ArrayList<Opinion>) :
    BaseAdapter() {
    private val inflater: LayoutInflater =
        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return dataSource.size
    }

    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val rowView = inflater.inflate(R.layout.listview_item, parent, false)

        val commentMsgTextView = rowView.findViewById(R.id.opinion_list_comment_msg) as TextView
        val ratingValueTextView =
            rowView.findViewById<RatingBar>(R.id.opinion_list_rating_msg) as RatingBar
        val opinion = getItem(position) as Opinion

        commentMsgTextView.text = opinion.message
        ratingValueTextView.rating = opinion.rating

        return rowView
    }
}