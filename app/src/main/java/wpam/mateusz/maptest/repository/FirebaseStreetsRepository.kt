package wpam.mateusz.maptest.repository

import android.graphics.Path
import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import wpam.mateusz.maptest.model.Opinion
import wpam.mateusz.maptest.model.Street

class FirebaseStreetsRepository {
    companion object {
        val database = FirebaseDatabase.getInstance().reference
        private val TAG = FirebaseStreetsRepository::class.java.simpleName

        fun streetExists(id: Int, exists_fun: (Street) -> Unit, notexists_fun: () -> Unit) {
            val ref = database.child("streets")

            ref.addListenerForSingleValueEvent(
                object : ValueEventListener {
                    override fun onCancelled(databaseError: DatabaseError) {
                        Log.w("TAG", "streetLoad: onCancelled", databaseError.toException())
                    }

                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        val streetData = dataSnapshot.child(id.toString())
                        val street = streetData.getValue(Street::class.java)
                        if(street != null) {
                            Log.w("TAG", "streetExists: " + street.toString())
                            exists_fun(street)
                            return
                        }
                        else {
                            notexists_fun()
                        }
                    }
                }
            )
        }

        fun addStreet(street: Street) {
            database.child("streets").child(street.id.toString()).setValue(street)
        }



        fun addOpinion(streetId: Int, opinion: Opinion) {
            val key = database.child("streets").child(streetId.toString()).child("opinions").push().key
            if (key == null) {
                Log.w(TAG, "Couldn't get push key for posts")
                return
            }

            database.child("streets").child(streetId.toString()).child("opinions").child(key).setValue(opinion)

            database.child("streets").child(streetId.toString()).addListenerForSingleValueEvent(object: ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    var ratingSum = 0f

                    val dataOpinions = dataSnapshot.child("opinions")
                    for (data in dataOpinions.children) {
                        val opinion = data.getValue(Opinion::class.java)
                        if (opinion != null) {
                            ratingSum += opinion.rating
                        }
                    }

                    val averageRating = ratingSum / dataOpinions.childrenCount
                    updateStreetRating(streetId, averageRating)

                }

                override fun onCancelled(databaseError: DatabaseError) {
                    Log.w(TAG, "Failed while updating street rating: ", databaseError.toException())                }
            })
        }

        fun updateStreetRating(streetId: Int, averageRating: Float) {
            database.child("streets").child(streetId.toString()).child("rating").setValue(averageRating)

        }
    }
}