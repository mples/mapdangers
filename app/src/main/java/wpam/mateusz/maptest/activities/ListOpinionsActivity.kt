package wpam.mateusz.maptest.activities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock
import android.util.Log
import android.widget.ListView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import wpam.mateusz.maptest.R
import wpam.mateusz.maptest.adapters.OpinionAdapter
import wpam.mateusz.maptest.model.Opinion

class ListOpinionsActivity : AppCompatActivity() {
    private var selectedStreetId: Int? = null
    private val ADD_REQUEST_CODE = 1
    private val database = FirebaseDatabase.getInstance().reference
    private val TAG = ListOpinionsActivity::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_opinions)
        selectedStreetId =
            intent.getSerializableExtra(AlarmClock.EXTRA_MESSAGE)?.toString()?.toInt()
        val opinionsListView = findViewById<ListView>(R.id.opinionsListView)
        val ctx = this

        database.child("streets").child(selectedStreetId.toString()).child("opinions")
            .addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val retrievedOpinionList = arrayListOf<Opinion>()
                    for (data in dataSnapshot.children) {
                        val opinion = data.getValue(Opinion::class.java)
                        if (opinion != null) {
                            retrievedOpinionList.add(opinion)
                        }
                    }
                    val opAdapter = OpinionAdapter(ctx, retrievedOpinionList)
                    opinionsListView.adapter = opAdapter

                }

                override fun onCancelled(databaseError: DatabaseError) {
                    Log.w(TAG, "Failed to get opinionsCount: ", databaseError.toException())
                }
            })

        val addOpinionButton = findViewById<FloatingActionButton>(R.id.addOpinionButton)

        addOpinionButton.setOnClickListener {
            val intent = Intent(this, AddOpinionActivity::class.java).apply {
                putExtra(AlarmClock.EXTRA_MESSAGE, selectedStreetId)
            }
            startActivityForResult(intent, ADD_REQUEST_CODE)
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            val intent = Intent(this, ListOpinionsActivity::class.java).apply {
                putExtra(AlarmClock.EXTRA_MESSAGE, selectedStreetId)
            }
            startActivity(intent)
            this.finish()
        }
    }
}
