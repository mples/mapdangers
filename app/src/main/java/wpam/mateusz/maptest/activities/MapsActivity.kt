package wpam.mateusz.maptest.activities

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Resources
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import java.util.*
import android.provider.AlarmClock
import com.google.android.gms.common.api.Status
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.AddressComponents
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.google.firebase.database.*
import wpam.mateusz.maptest.R
import wpam.mateusz.maptest.StreetInfoFragment
import wpam.mateusz.maptest.common.Utils
import wpam.mateusz.maptest.model.Street
import wpam.mateusz.maptest.model.StreetAddress
import wpam.mateusz.maptest.repository.FirebaseStreetsRepository


class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var map: GoogleMap

    private val TAG = MapsActivity::class.java.simpleName

    private val REQUEST_LOCATION_PERMISSION = 1

    private var selectedStreetId: Int? = null

    private var isStreetInfoFragmentOpen = false

    private lateinit var placesClient: PlacesClient

    private lateinit var database: DatabaseReference

    private val closeZoomLevel: Float = 15.0f

    private val farZoomLevel: Float = 12.0f

    private val homeLatLng = LatLng(52.2275638, 21.0228191)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        initPlaces()

        selectedStreetId =
            intent.getSerializableExtra(AlarmClock.EXTRA_MESSAGE)?.toString()?.toInt()

        database = FirebaseDatabase.getInstance().reference
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(homeLatLng, farZoomLevel))
        setMapStyle(map)
        enableMyLocation()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.map_option, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.normal_map -> {
            map.mapType = GoogleMap.MAP_TYPE_NORMAL
            true
        }
        R.id.hybrid_map -> {
            map.mapType = GoogleMap.MAP_TYPE_HYBRID
            true
        }
        R.id.satellite_map -> {
            map.mapType = GoogleMap.MAP_TYPE_SATELLITE
            true
        }
        R.id.terrain_map -> {
            map.mapType = GoogleMap.MAP_TYPE_TERRAIN
            true
        }
        else -> super.onOptionsItemSelected(item)
    }


    private fun setMapStyle(map: GoogleMap) {
        try {
            val success = map.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(this, R.raw.map_style)
            )
            if (!success) {
                Log.e(TAG, "Style parsing failed.")
            }
        } catch (e: Resources.NotFoundException) {
            Log.e(TAG, "Style parsing failed.", e)
        }
    }

    private fun isPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            android.Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun enableMyLocation() {
        if (isPermissionGranted()) {
            map.isMyLocationEnabled = true
        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf<String>(android.Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_LOCATION_PERMISSION
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            if (grantResults.contains(PackageManager.PERMISSION_GRANTED)) {
                enableMyLocation()
            }
        }
    }

    private fun initPlaces() {
        val ctx = this
        Places.initialize(applicationContext, "AIzaSyABi_r5c-a-nYwl-w9ge28s-7PB5KEOHUo")

        placesClient = Places.createClient(this)


        val autocompleteFragment: AutocompleteSupportFragment =
            supportFragmentManager.findFragmentById(R.id.autocomplete_fragment) as AutocompleteSupportFragment

        autocompleteFragment.setPlaceFields(
            Arrays.asList(
                Place.Field.ID,
                Place.Field.ADDRESS_COMPONENTS,
                Place.Field.LAT_LNG
            )
        )

        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {
                val streetAddress = parseAddressComponentToStreetAddress(place.addressComponents)
                val streetLatLng = place.latLng
                if (streetAddress != null && streetLatLng != null) {
                    val id = Utils.generateStreetId(streetAddress)
                    FirebaseStreetsRepository.streetExists(id, {
                        val latLng = LatLng(it.latitude, it.longitude)
                        map.moveCamera(
                            CameraUpdateFactory.newLatLngZoom(
                                latLng,
                                closeZoomLevel
                            )
                        )
                        map.addMarker(
                            MarkerOptions()
                                .position(latLng)
                                .title(it.address.name)
                                .icon(
                                    BitmapDescriptorFactory.defaultMarker(
                                        BitmapDescriptorFactory.HUE_RED
                                    )
                                )
                        )

                        closeStreetInfoFragment()
                        openStreetInfoFragment(it.id)
                    },
                        {
                            FirebaseStreetsRepository.addStreet(
                                Street(
                                    streetAddress,
                                    streetLatLng.latitude,
                                    streetLatLng.longitude,
                                    0f,
                                    id
                                )
                            )
                            selectedStreetId = id
                            map.moveCamera(
                                CameraUpdateFactory.newLatLngZoom(
                                    streetLatLng,
                                    closeZoomLevel
                                )
                            )
                            map.addMarker(
                                MarkerOptions()
                                    .position(streetLatLng)
                                    .title(streetAddress.name)
                                    .icon(
                                        BitmapDescriptorFactory.defaultMarker(
                                            BitmapDescriptorFactory.HUE_RED
                                        )
                                    )
                            )

                            closeStreetInfoFragment()
                            openStreetInfoFragment(id)
                        })

                } else {
                    //TODO add dialog to select coorect street
                    val builder: AlertDialog.Builder? = AlertDialog.Builder(ctx)


                    builder
                        ?.setMessage(R.string.dialog_message)
                        ?.setTitle(R.string.dialog_title)
                        ?.setPositiveButton(
                            R.string.dialog_button_ok,
                            DialogInterface.OnClickListener { dialog, id ->
                                dialog.dismiss()
                            })

                    val dialog: AlertDialog? = builder?.create()

                    dialog?.show()

                }

            }

            override fun onError(status: Status) {
                Log.i(TAG, "An error occurred while selecting place: $status")
            }
        })
    }


    private fun parseAddressComponentToStreetAddress(addressComponents: AddressComponents?): StreetAddress? {
        val addressList = addressComponents?.asList()
        var route: String? = null
        var city: String? = null
        var country: String? = null
        if (addressList != null) {
            for (comp in addressList) {
                if ("route" in comp.types) {
                    route = comp.name
                }
                if ("locality" in comp.types) {
                    city = comp.name
                }
                if ("country" in comp.types) {
                    country = comp.name
                }
            }
        }

        if (route != null && city != null && country != null) {
            return StreetAddress(route, city, country)
        }
        return null
    }


    private fun openStreetInfoFragment(streetId: Int) {
        if (!isStreetInfoFragmentOpen) {
            val fragment = StreetInfoFragment()
            val fragmentManager = supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragment.selectedStreet = streetId
            fragmentTransaction.add(R.id.fragment_container, fragment).addToBackStack(null).commit()
            isStreetInfoFragmentOpen = true
        }
    }

    private fun closeStreetInfoFragment() {
        if (isStreetInfoFragmentOpen) {
            val fragmentManager = supportFragmentManager
            val fragment =
                fragmentManager.findFragmentById(R.id.fragment_container) as StreetInfoFragment
            fragmentManager.beginTransaction().remove(fragment).commit()
            isStreetInfoFragmentOpen = false
        }
    }
}
