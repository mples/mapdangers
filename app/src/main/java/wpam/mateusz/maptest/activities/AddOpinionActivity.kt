package wpam.mateusz.maptest.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock
import android.view.View
import android.widget.EditText
import android.widget.RatingBar
import android.app.Activity
import android.view.inputmethod.InputMethodManager
import wpam.mateusz.maptest.R
import wpam.mateusz.maptest.model.Opinion
import wpam.mateusz.maptest.repository.FirebaseStreetsRepository


class AddOpinionActivity : AppCompatActivity() {
    private var selectedStreetId: Int? = null
    private lateinit var commentEditText: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_opinion)
        selectedStreetId =
            intent.getSerializableExtra(AlarmClock.EXTRA_MESSAGE)?.toString()?.toInt()
        commentEditText = findViewById(R.id.commentEditText)
        commentEditText.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                hideKeyboard(v)
            }
        }
    }

    fun addNewOpinion(view: View) {
        val id = selectedStreetId
        if (id != null) {
            val ratingBar = findViewById<RatingBar>(R.id.ratingBar)
            val streetRating = ratingBar.rating
            val commentText = findViewById<EditText>(R.id.commentEditText)
            val message = commentText.text.toString()
            val opinion = Opinion(message, streetRating)

            val id = selectedStreetId
            if (id != null) {
                FirebaseStreetsRepository.addOpinion(id, opinion)
            }
            setResult(Activity.RESULT_OK, null)
            finish()
        }

    }

    private fun hideKeyboard(view: View) {
        val inputMethodManager =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager

        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

}
