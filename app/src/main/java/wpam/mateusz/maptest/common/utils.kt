package wpam.mateusz.maptest.common

import wpam.mateusz.maptest.model.StreetAddress


class Utils() {
    companion object {
        fun generateStreetId(address: StreetAddress): Int {
            return address.name.hashCode() and address.city.hashCode() and address.county.hashCode()
        }
    }

}