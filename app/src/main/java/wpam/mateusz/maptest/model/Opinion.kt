package wpam.mateusz.maptest.model

data class Opinion(
    val message: String = "",
    val rating: Float = 0.0f
)