package wpam.mateusz.maptest.model

import com.google.firebase.database.Exclude

data class Street(
    val address: StreetAddress = StreetAddress("", "", ""),
    val latitude: Double = 0.0,
    val longitude: Double = 0.0,
    var rating: Float = 0f,
    val id: Int = 0
)
