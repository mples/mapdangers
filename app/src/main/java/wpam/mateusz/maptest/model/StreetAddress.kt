package wpam.mateusz.maptest.model

data class StreetAddress(
    val name: String = "",
    val city: String = "",
    val county: String = ""
)
